const express = require('express')
const hello_world = require('./hello_world')

const app = express()

app.get('/', function(req, res) {
    res.send({
        "message": hello_world.get_message()
    })
})

module.exports = app
