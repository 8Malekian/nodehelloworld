const app = require('./server')
const supertest = require('supertest')
const hello_world = require('./hello_world')

const request = supertest(app)

test('Server runs well and returns the home page', async done => {
    const response = await request.get('/')

    expect(response.status).toBe(200)
    expect(response.body.message).toBe(hello_world.get_message())
    done()
})
